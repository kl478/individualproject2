use actix_files as fs;
use actix_web::{web, App, HttpResponse, HttpServer, Responder};
use serde::Deserialize;

#[derive(Deserialize)]
struct Guess {
    door: String,
}

async fn index() -> impl Responder {
    fs::NamedFile::open("index.html").unwrap()
}

async fn guess_door(query: web::Query<Guess>) -> impl Responder {
    let car_door = "2";
    let car_emoji = "\u{1F697}";
    let goat_emoji = "\u{1F410}";

    if query.door == car_door {
        HttpResponse::Ok()
            .content_type("text/html; charset=utf-8")
            .body(format!("{} Congratulations! You found the car.", car_emoji))
    } else {
        HttpResponse::Ok()
            .content_type("text/html; charset=utf-8")
            .body(format!(
                "{} Sorry, no car behind this door. Try again!",
                goat_emoji
            ))
    }
}

#[actix_web::main]
async fn main() -> std::io::Result<()> {
    HttpServer::new(|| {
        App::new()
            .route("/", web::get().to(index))
            .route("/guess", web::get().to(guess_door))
            .service(fs::Files::new("/", "./static").index_file("index.html"))
    })
    .bind("0.0.0.0:8080")?
    .run()
    .await
}
