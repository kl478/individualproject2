# Individual Project 2: Find the Car - Rust Actix Web App

[Demo video](https://gitlab.com/kl478/individualproject2/-/blob/main/screenshots/demo.mov?ref_type=heads)

## Project Description

This project is a Rust-based web application utilizing the `Actix web` framework. It's designed to be containerized using `Docker` for easy deployment and scaling. 

### Features
The application features a simple game where users can guess which of the three doors hides a car. Each button represents a door. When the user clicks a button, a page with the information of whether the user guessed correctly will show up.

## Technical Goals Achieved
- **Rust-Based Web Service**: Utilizes Rust and the Actix Web framework to implement a RESTful API. This app exposes a simple REST API where users can make `GET` requests (e.g., to `/guess` with a query parameter specifying a door).
- **Containerization**: The application is fully containerized with `Docker`, facilitating deployment and scaling across environments.
- **CI/CD Integration**: Includes a comprehensive `.gitlab-ci.yml` for continuous integration and deployment, automating the build, test, and deploy process.

## Steps
1. Ensure you have `Docker Desktop` installed and running.

2. Create a New Cargo Project using:
```bash
cargo new <YOUR PROJECT NAME>
```

3. Edit `Cargo.toml` to include the following dependencies (and other dependencies necessary for your APP):
    - `actix-web`: Rust web framework
    - `actix-files`: Handle serving static files

4. Implement the web APP in `src/main.rs`.
    - Include `use actix_web::{web, App, HttpResponse, HttpServer, Responder}`
    - Include `use actix_files as fs`
    - Write your functions for your web app

5. Create a `index.html`, the HTML file for your application's frontend, in the root directory.

6. Update the `main.rs` to serve static files and set `index.html` as the index file:

```rust
.service(fs::Files::new("/", "./static").index_file("index.html"))
```

7. Create a `Dockerfile` in the root directory. This file contains instructions for Docker how to build the application image.
    - Make sure the name of the project for the `Dockerfile` and the IMAGE_NAME in the `Makefile` match the name in `Cargo.toml`.

8. `cd` to your project directory, then build and run with Cargo with the following commands:
```bash
make all

``` 

```bash
cargo run
```

9. The application will be accessible at http://localhost:8080.

10. With `Docker Desktop` open, build the Docker image and then run the Docker container using the provided `Makefile`:
```bash
make docker-build

``` 
```bash
make docker-run

``` 

11. You can verify the container running using `Docker Desktop` or by executing the command `docker ps` in the terminal, which lists all active containers. Look for your application's container in the list.

12. To view your Actix web application, open a web browser and navigate to `http://localhost:8080`. This port is mapped to the exposed port in the Docker container, allowing you to interact with the web application running within Docker.

## Screenshots

### Web App Screenshots
![homepage](screenshots/homepage.png)

#### Enter door 1
![door 1](screenshots/door1.png)

#### Enter door 2
![door 2](screenshots/door2.png)

#### Enter door 3
![door 3](screenshots/door3.png)

### Docker Container Screenshots
![docker container 1](screenshots/container.png)

![docker container 2](screenshots/container1.png)

![docker image](screenshots/dockerImage.png)
