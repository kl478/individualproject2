# Use the official Rust image as the build environment
FROM rust:1-slim-bookworm AS builder

# Create a new empty shell project
RUN USER=root cargo new --bin miniproject4
WORKDIR /miniproject4

# Copy over your manifests
COPY ./Cargo.toml ./Cargo.toml
COPY ./Cargo.lock ./Cargo.lock

# This build step will cache your dependencies
RUN cargo build --release
RUN rm src/*.rs

# Copy the source code
COPY ./src ./src


# Build the application for release
RUN rm ./target/release/deps/miniproject4*
RUN cargo build --release

# Use the Debian Buster image as the runtime environment
FROM bitnami/minideb:bookworm

# Copy the build artifact from the build stage and set the working directory
COPY --from=builder /miniproject4/target/release/miniproject4 /usr/local/bin
COPY ./index.html /usr/local/bin
RUN ls -la /usr/local/bin/

# Set the environment to production
ENV ROCKET_ENV=production

# Expose port 8080
EXPOSE 8080

# Set the binary as the entrypoint of the container
ENTRYPOINT ["/usr/local/bin/miniproject4"]

# Run the binary
CMD ["./miniproject4"]

